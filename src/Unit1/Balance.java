package Unit1;
/*
Write a program that prints the balance of an account after the first, second, and
third year. The account has an initial balance of $1,000 and earns 5 percent interest
per year.
*/

public class Balance {

    public static void main(String[] args) {
        final double r = 0.05;
        double f = 1000.0;

        double a = ((f*r)+f);
        double b = a + ((f * r) + f);
        double c = a + b + ((f * r) + f);

        System.out.println(a);
        System.out.println(b);
        System.out.println(c);


    }
}
