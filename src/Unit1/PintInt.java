package Unit1;

/*
Write a program that prints the product of the first ten positive integers,
1 × 2 × ... × 10. (Use * to indicate multiplication in Java.)
*/

public class PintInt {
    public static void main(String[] args){
        int total = 1;
        for (int i = 1; i <= 10; i++) {
            total *= i;
        }
        System.out.println("Total is " + total);
    }
}

