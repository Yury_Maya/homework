package Unit1;
/*Write a program that prints a face similar to (but different from) the following:
  /////
 +"""""+
(| o o |)
 |  ^  |
 | ‘-’ |
 +-----+
*/

public class PintShape {
    public static void main(String[] args) {

        System.out.println("  /////   ");
        System.out.println(" +\"\"\"\"\"+");
        System.out.println("(| o o |) ");
        System.out.println(" |  ^  |  ");
        System.out.println(" | ‘-’ |  ");
        System.out.println(" +-----+  ");

    }
}



