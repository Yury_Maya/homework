package Unit1;

/*Write a program that prints a house that looks exactly like the following:
   +
  + +
 +   +
+-----+
| .-. |
| | | |
+-+-+-+
*/

public class PintTree {
    public static void main(String[] args) {

        System.out.println("   +   ");
        System.out.println("  + +  ");
        System.out.println(" +   + ");
        System.out.println("+-----+");
        System.out.println("| .-. |");
        System.out.println("| | | |");
        System.out.println("+-+-+-+");

    }
}
