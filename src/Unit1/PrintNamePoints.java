package Unit1;

/*
Write a program that prints your name in large letters, such as
*    *  *    *  ****    *   *
*    *  *    *  *   *   *   *
  * *   *    *  ****     * *
   *     *  *   *   *     *
   *      ***   *    *    *
*/


public class PrintNamePoints {
    public static void main(String[] args) {

        System.out.println("*    *  *    *  ****    *   *");
        System.out.println("*    *  *    *  *   *   *   *");
        System.out.println("  * *   *    *  ****     * * ");
        System.out.println("   *     *  *   *   *     *  ");
        System.out.println("   *      **    *    *    *  ");
    }
}


